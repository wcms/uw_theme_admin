/**
 * Ckeditor Modal
 */
(function ($, Drupal) {
  if ($.ui.dialog) {
    orig_allowInteraction = $.ui.dialog.prototype._allowInteraction;
    $.ui.dialog.prototype._allowInteraction = function (event) {
      if ($(event.target).closest('.cke_dialog').length) {
        return true;
      }
      return orig_allowInteraction.apply(this, arguments);
    };
  }
})(jQuery, Drupal);

/**
 * Adding required to banner images image field.
 */
(function ($, Drupal) {
  Drupal.behaviors.uwBannerImageRequire = {
    attach: function (context, settings) {
      $(document).ready(function() {
        if ($("[id^=field_uw_ban_image-media-library-wrapper]").hasClass('form-required')) {
          $("[id^=field_uw_ban_image-media-library-wrapper]").removeClass('form-required');
          $("[id^=field_uw_ban_image-media-library-wrapper] legend span").addClass('form-required');
        }
      });
    }
  };
})(jQuery, Drupal);
